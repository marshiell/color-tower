using GameAnalyticsSDK;
using UnityEngine;

public class GameAnalyticsInitialize : MonoBehaviour
{
    void Start()
    {
        GameAnalytics.Initialize();
    }
}

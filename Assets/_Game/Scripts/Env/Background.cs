using UnityEngine;

public class Background : MonoBehaviour
{
    private int currentBackgroundWorldId;
    private GameObject instantiatedBackground;

    public void SetBackground(GameObject go, int worldId)
    {
        if (currentBackgroundWorldId != worldId)
        {
            currentBackgroundWorldId = worldId;

            if (instantiatedBackground != null) Destroy(instantiatedBackground);
            if (go != null) instantiatedBackground = Instantiate(go, transform);
        }
    }
}

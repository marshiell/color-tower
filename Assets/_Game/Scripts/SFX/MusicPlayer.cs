using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    [SerializeField] AudioClip menuMusic;
    [SerializeField] AudioClip[] worldMusics;
    private AudioSource audioSource;
    private bool inGameMusicStopped = true;

    int songLoaded = -1;
    bool newMusicLoaded;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        UpdateVolume();
    }

    public void PlayMenu()
    {
        if (audioSource == null) audioSource = GetComponent<AudioSource>();
        audioSource.clip = menuMusic;
        if (!audioSource.isPlaying)
        {
            audioSource.Play();
            songLoaded = -1;
        }
    }

    public void PlayOrUnpause()
    {
        LoadSongWorld(PlayerPrefs.GetInt("CurrentWorldId", 1));
        if (inGameMusicStopped || newMusicLoaded)
        {
            newMusicLoaded = false;
            audioSource.Play();
        }
        else
            audioSource.UnPause();

        inGameMusicStopped = false;
    }

    public void Pause()
    {
        audioSource.Pause();
    }

    public void Stop()
    {
        audioSource.Stop();
        inGameMusicStopped = true;
    }

    public void LoadSongWorld(int worldId)
    {
        if (songLoaded != worldId)
        {
            audioSource.clip = worldMusics[worldId - 1];
            newMusicLoaded = true;
        }
        songLoaded = worldId;
    }

    public void UpdateVolume()
    {
        if (IsMusicSettingOn())
            audioSource.volume = 1;
        else
            audioSource.volume = 0;
    }

    public static bool IsSoundSettingOn()
    {
        return PlayerPrefs.GetInt("sound") == 0;
    }

    public static bool IsMusicSettingOn()
    {
        return PlayerPrefs.GetInt("music") == 0;
    }
}

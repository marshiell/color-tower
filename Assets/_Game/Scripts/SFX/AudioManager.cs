using UnityEngine;

public class AudioManager : MonoBehaviour
{
    void Start()
    {
        UpdateAudioListenerState();
    }

    public void UpdateAudioListenerState()
    {
        AudioListener.volume = MusicPlayer.IsSoundSettingOn() ? PlayerPrefs.GetFloat("AudioVolume", 1.0f) : 0;
    }
}

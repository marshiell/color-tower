﻿using UnityEngine;
using UnityEngine.UI;

public class SoundButton : MonoBehaviour
{
    public Sprite soundOn;
    public Sprite soundOff;

    [SerializeField] Image soundImage;

    void Start()
    {
        applyTextureFromSave();
    }

    public void applyTextureFromSave()
    {
        if (MusicPlayer.IsSoundSettingOn())
            soundImage.sprite = soundOn;
        else
            soundImage.sprite = soundOff;
    }

    public void toggleSound()
    {
        if (MusicPlayer.IsSoundSettingOn())
            PlayerPrefs.SetInt("sound", 1);
        else
            PlayerPrefs.SetInt("sound", 0);

        PlayerPrefs.Save();

        var audioManager = FindObjectOfType<AudioManager>();
        audioManager.UpdateAudioListenerState();

        applyTextureFromSave();
    }
}

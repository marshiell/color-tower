using UnityEngine;
using UnityEngine.UI;

public class VolumeSlider : MonoBehaviour
{
    Slider slider;

    private void OnEnable()
    {
        if (slider == null) slider = GetComponent<Slider>();

        slider.value = PlayerPrefs.GetFloat("AudioVolume", 1.0f);
    }

    public void OnVolumeChange()
    {
        if (slider == null) slider = GetComponent<Slider>();

        if (MusicPlayer.IsSoundSettingOn()) 
            AudioListener.volume = slider.value;

        PlayerPrefs.SetFloat("AudioVolume", slider.value);
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

public class MusicButton : MonoBehaviour
{
    public Sprite musicOn;
    public Sprite musicOff;

    [SerializeField] Image soundImage;

    private MusicPlayer musicPlayer;

    void Start()
    {
        musicPlayer = FindObjectOfType<MusicPlayer>();
        applyTextureFromSave();
    }

    public void applyTextureFromSave()
    {
        if (MusicPlayer.IsMusicSettingOn())
            soundImage.sprite = musicOn;
        else
            soundImage.sprite = musicOff;
    }

    public void ToggleMusic()
    {
        if (MusicPlayer.IsMusicSettingOn())
            PlayerPrefs.SetInt("music", 1);
        else
            PlayerPrefs.SetInt("music", 0);

        PlayerPrefs.Save();

        applyTextureFromSave();

        musicPlayer.UpdateVolume();
    }
}

using UnityEngine;

public class LevelControllerSingleton : MonoBehaviour
{
    private static LevelController levelController;
    public static LevelController GetLevelController()
    {
        if (levelController == null)
            levelController = FindObjectOfType<LevelController>();
        return levelController;
    }
}

public class LevelController : MonoBehaviour
{
    private int currentWorldId;
    public int CurrentWorldId { get { return currentWorldId; } set { currentWorldId = value; } }


    public WorldScriptableObject[] Worlds;
    private WorldScriptableObject currentWorld;
    public WorldScriptableObject CurrentWorld { get { return currentWorld; } }
    private void Awake()
    {
        currentWorldId = PlayerPrefs.GetInt("CurrentWorldId", 1);
        currentWorld = Worlds[currentWorldId];
        currentWorld.InitializeWorld();
        UpdateLevelIndicators();
    }

    public bool IsLastLevel()
    {
        return currentWorldId == Worlds.Length - 1;
    }
    public void GoToNextLevel()
    {
        currentWorldId++;

        if (currentWorldId >= Worlds.Length)
            currentWorldId = 0;

        PlayerPrefs.SetInt("CurrentWorldId", currentWorldId);

        currentWorld = Worlds[currentWorldId];
        currentWorld.InitializeWorld();

        UpdateLevelIndicators();
    }

    void UpdateLevelIndicators()
    {
        var levelIndicators = FindObjectsOfType<LevelIndicator>();
        foreach (var levelIndicator in levelIndicators)
            levelIndicator.UpdateText(currentWorld.WorldId);
    }
}

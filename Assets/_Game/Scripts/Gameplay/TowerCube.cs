using UnityEngine;

public class TowerCube : MonoBehaviour
{
    private Color currentColor;
    public Color CurrentColor { get { return currentColor; } }

    Renderer rendererComponent;
    private void Start()
    {
        UpdateMaterial();
    }
    public void UpdateMaterial(Material material = null)
    {
        if (rendererComponent == null) rendererComponent = GetComponent<Renderer>();
        if (material != null) rendererComponent.material = material;
        currentColor = rendererComponent.material.color;
    }
    public void CollideWithSphere(Sphere sphere)
    {
        transform.parent.GetComponent<Tower>().CollideWithSphere(sphere, currentColor == sphere.GetComponent<Renderer>().material.color);
    }
    void OnTriggerEnter(Collider other)
    {
        var goalLine = other.gameObject.GetComponent<GoalLine>();
        if (goalLine != null)
        {
            Debug.Log("Win!");
            FindObjectOfType<GameController>().Win();
        }
    }
}

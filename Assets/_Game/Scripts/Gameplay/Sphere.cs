using UnityEngine;

public enum PowerUpType
{
    None,
    ChangeSize,
    SlowMotion
}

public class Sphere : MonoBehaviour
{
    [SerializeField] Vector3 MovementDirection = Vector3.one;
    [SerializeField] float MovementSpeed = 1.0f;

    [SerializeField] PowerUpType PowerUpType;
    [SerializeField] float Duration = 2.0f;
    [SerializeField] float Value = 1.0f;

    bool AlreadyCollided;
    TimeController timeController;
    private void Start()
    {
        timeController = FindObjectOfType<TimeController>();
    }
    void Update()
    {
        transform.Translate(MovementDirection * Time.deltaTime * MovementSpeed * timeController.TimeScale);
    }

    public void ApplyPowerUp(Tower tower)
    {
        switch(PowerUpType)
        {
            case PowerUpType.None:
            default:
                break;
            case PowerUpType.ChangeSize:
                tower.ChangeScaleForDuration(Value, Duration);
                break;
            case PowerUpType.SlowMotion:
                timeController.ChangeTimeScaleForDuration(Value, Duration);
                break;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (AlreadyCollided) return;

        var towerCube = other.gameObject.GetComponent<TowerCube>();
        if (towerCube != null)
        {
            AlreadyCollided = true;
            towerCube.CollideWithSphere(this);
            Destroy(gameObject);
        }
        
        var bottomLimit = other.gameObject.GetComponent<BottomLimit>();
        if (bottomLimit != null)
        {
            Destroy(gameObject);
        }

    }
}

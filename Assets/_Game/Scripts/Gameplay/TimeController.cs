using System.Collections;
using UnityEngine;

public class TimeController : MonoBehaviour
{
    private float timeScale = 1.0f;
    public float TimeScale { get { return timeScale; } }

    private float startTimeScale;

    private void Start()
    {
        startTimeScale = timeScale;
    }

    public void ChangeTimeScaleForDuration(float timeScale, float duration)
    {
        StartCoroutine(ChangeTimeScaleForDurationAndReset(timeScale, duration));
    }

    IEnumerator ChangeTimeScaleForDurationAndReset(float timeScale, float duration)
    {
        this.timeScale = timeScale;
        yield return new WaitForSeconds(duration);
        this.timeScale = startTimeScale;
        yield return null;
    }
}

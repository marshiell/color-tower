using System.Collections;
using UnityEngine;

public class Tower : MonoBehaviour
{
    [SerializeField] GameObject TowerCubePrefab;
    [SerializeField] bool CantPickSameColorAgain = true;

    int towerCubeCount;
    public int TowerCubeCount { get { return towerCubeCount; } }

    GameObject topTowerCube;
    public GameObject TopTowerCube { get { return topTowerCube; } }

    GameController gameController;
    VisibleScreen visibleScreen;

    Vector3 startPosition;
    Vector3 startScale;
    int startTowerCubeCount;
    private void Awake()
    {
        gameController = FindObjectOfType<GameController>();
        visibleScreen = FindObjectOfType<VisibleScreen>();
        towerCubeCount = transform.childCount;
        startTowerCubeCount = towerCubeCount;
        topTowerCube = transform.GetChild(towerCubeCount - 1).gameObject;

        startPosition = transform.position;
        startScale = transform.localScale;
    }

    public void ResetTower()
    {
        transform.position = startPosition;
        transform.localScale = startScale;

        while (towerCubeCount != startTowerCubeCount)
        {
            if (towerCubeCount < startTowerCubeCount)
                AddTowerCube(UpdateRandomMaterial());
            else RemoveTowerCube();
        }
    }

    public void CollideWithSphere(Sphere sphere, bool correctColor)
    {
        if (correctColor)
            CollideWithCorrectSphere();
        else RemoveTowerCube();

        sphere.ApplyPowerUp(this);
    }
    void RemoveTowerCube()
    {
        Destroy(topTowerCube);
        towerCubeCount--;

        if (towerCubeCount <= 0)
        {
            Debug.Log("Lose!");
            Destroy(gameObject);

            FindObjectOfType<GameController>().Lose();
        }
        else
            topTowerCube = transform.GetChild(towerCubeCount - 1).gameObject;

        visibleScreen.TowerSizeChanged(this, -1);
    }

    void CollideWithCorrectSphere()
    {
        var randomMaterial = UpdateRandomMaterial();
        AddTowerCube(randomMaterial);
    }

    void AddTowerCube(Material randomMaterial)
    {
        topTowerCube = Instantiate(TowerCubePrefab, Vector3.zero, Quaternion.identity, transform);
        topTowerCube.transform.localPosition = new Vector3(0, towerCubeCount, 0);
        topTowerCube.GetComponent<TowerCube>().UpdateMaterial(randomMaterial);
        towerCubeCount++;

        visibleScreen.TowerSizeChanged(this, 1);
    }

    Material UpdateRandomMaterial()
    {
        var randomMaterial = PickRandomMaterial();

        var towerCubes = FindObjectsOfType<TowerCube>();
        foreach (var towerCube in towerCubes)
            towerCube.UpdateMaterial(randomMaterial);

        return randomMaterial;
    }

    Material PickRandomMaterial()
    {
        var currentColor = FindObjectOfType<TowerCube>().CurrentColor;
        var randomMaterial = gameController.Materials[Random.Range(0, gameController.Materials.Length)];
        const int maximumIterationsAmount = 100;

        int i = 0;
        while(CantPickSameColorAgain && i < maximumIterationsAmount && currentColor == randomMaterial.color)
        {
            randomMaterial = gameController.Materials[Random.Range(0, gameController.Materials.Length)];
            i++;
        }

        return randomMaterial;
    }
    public void ChangeScaleForDuration(float scale, float duration)
    {
        StartCoroutine(ChangeScaleForDurationAndReset(scale, duration));
    }

    IEnumerator ChangeScaleForDurationAndReset(float scale, float duration)
    {
        transform.LeanScale(new Vector3(startScale.x * scale, startScale.y, startScale.z), .5f).setEaseOutBack();
        yield return new WaitForSeconds(duration);
        transform.LeanScale(startScale, .5f).setEaseOutBack();
        yield return null;
    }
}

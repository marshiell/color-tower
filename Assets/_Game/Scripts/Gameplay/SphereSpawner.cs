using System.Collections;
using UnityEngine;

public class SphereSpawner : MonoBehaviour
{
    [SerializeField] float IntervalBetweenSpawns;

    Vector3 lastSpawnCoordinates;

    GameController gameController;
    Tower tower;
    void Start()
    {
        gameController = FindObjectOfType<GameController>();
        tower = FindObjectOfType<Tower>();
    }

    public void ActivateSpawning()
    {
        StartCoroutine(SpawnSphereCoroutine());
    }

    IEnumerator SpawnSphereCoroutine()
    {
        while (gameController.Playing)
        {
            yield return new WaitForSeconds(IntervalBetweenSpawns);
            if (gameController.Playing)
                InstantiateSphere();
        }
    }

    void InstantiateSphere()
    {
        var spherePrefab = LevelControllerSingleton.GetLevelController().CurrentWorld.spheres[Random.Range(0, LevelControllerSingleton.GetLevelController().CurrentWorld.spheres.Length)];
        lastSpawnCoordinates = ComputeRandomSpawnCoordinatesWithMinimumDistanceToLastSpawnCoordinates(lastSpawnCoordinates, tower.transform.localScale.x + spherePrefab.transform.localScale.x * 2);
        var sphereInstance = Instantiate(spherePrefab, lastSpawnCoordinates, Quaternion.identity);
        sphereInstance.GetComponent<Renderer>().material = gameController.Materials[Random.Range(0, gameController.Materials.Length)];
    }

    Vector3 ComputeRandomSpawnCoordinatesWithMinimumDistanceToLastSpawnCoordinates(Vector3 lastSpawnCoordinates, float minimalDistance)
    {
        Vector3 coordinates;
        const int maximumIterations = 100;
        int i = 0;

        do
        {
            coordinates = ComputeRandomSpawnCoordinates();
            i++;
        } while (Mathf.Abs(coordinates.x - lastSpawnCoordinates.x) < minimalDistance && i < maximumIterations);

        return coordinates;
    }

    Vector3 ComputeRandomSpawnCoordinates()
    {
        return transform.position + new Vector3(Random.Range(-transform.localScale.x / 2, transform.localScale.x / 2), 0, 0);
    }
}

using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] Material[] materials;
    public Material[] Materials { get { return materials; } }
    [SerializeField] GameObject WinFx;
    [SerializeField] float DelayToRestartGameAfterGameEnd;

    VisibleScreen visibleScreen;
    SphereSpawner spawner;
    MenuController menuController;
    Tower tower;
    bool playing;
    public bool Playing { get { return playing; } }
    void Start()
    {
        spawner = FindObjectOfType<SphereSpawner>();
        visibleScreen = FindObjectOfType<VisibleScreen>();
        tower = FindObjectOfType<Tower>();
        menuController = FindObjectOfType<MenuController>();
    }
    public void StartNextWorld()
    {
        ResetGameBoard();

        if (!LevelControllerSingleton.GetLevelController().IsLastLevel())
        {
            LevelControllerSingleton.GetLevelController().GoToNextLevel();
            StartGame();
        }
        else
        {
            menuController.LoadMenu(Menu.Main);
        }
    }

    void ResetGameBoard()
    {
        tower.ResetTower();
        visibleScreen.ResetScreenPosition();
        playing = false;

        DestroySpheres();

        WinFx.SetActive(false);
    }

    void DestroySpheres()
    {
        var spheres = FindObjectsOfType<Sphere>();
        foreach (var sphere in spheres)
            Destroy(sphere.gameObject);
    }
    public void StartGame()
    {
        playing = true;
        spawner.ActivateSpawning();
        visibleScreen.TowerSizeChanged(tower, 1);

        menuController.LoadMenu(Menu.InGame);
    }
    public void Win()
    {
        DestroySpheres();
        WinFx.SetActive(true);

        StartCoroutine(StartNextWorldAfterDelay(DelayToRestartGameAfterGameEnd));
        playing = false;

        menuController.LoadMenu(Menu.Win);
    }

    public void Lose()
    {
        StartCoroutine(ReloadScene(DelayToRestartGameAfterGameEnd));
        playing = false;
    }

    public void QuitGame()
    {
        DestroySpheres();
        playing = false;
    }

    IEnumerator ReloadScene(float delay)
    {
        yield return new WaitForSeconds(delay);
        Scene scene = SceneManager.GetActiveScene(); 
        SceneManager.LoadScene(scene.name);
    }

    IEnumerator StartNextWorldAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        StartNextWorld();
    }
}

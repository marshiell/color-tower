using UnityEngine;

public class VisibleScreen : MonoBehaviour
{
    [SerializeField] Camera cam;
    [SerializeField] int moveUpScreenAtPercent = 50;
    [SerializeField] int moveDownScreenAtPercent = 30;
    [SerializeField] float moveSpeed = 1.0f;
    [SerializeField] float yOffset = 300;
    float lowestY = 0;
    Vector3 startPosition;
    private void Start()
    {
        startPosition = transform.position;
        lowestY = transform.position.y;
    }

    public void ResetScreenPosition()
    {
        transform.position = startPosition;
    }

    public void TowerSizeChanged(Tower tower, int direction)
    {
        float topTowerCubeY = cam.WorldToScreenPoint(tower.TopTowerCube.transform.position).y;

        if (direction > 0 && topTowerCubeY / Screen.height * 100 > moveUpScreenAtPercent)
        {
            FollowTopTowerCubeYPosition(tower.TopTowerCube.transform.position.y + yOffset);
        }
        else if (direction < 0 && topTowerCubeY / Screen.height * 100 < moveDownScreenAtPercent)
        {
            FollowTopTowerCubeYPosition(tower.TopTowerCube.transform.position.y + yOffset);
        }
    }

    void FollowTopTowerCubeYPosition(float yPosition)
    {
        if (yPosition < lowestY) yPosition = lowestY;
        transform.LeanMoveLocalY(yPosition, moveSpeed);
    }
}

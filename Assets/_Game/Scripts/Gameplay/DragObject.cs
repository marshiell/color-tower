using UnityEngine;

public class DragObject : MonoBehaviour
{
    [SerializeField] float DragSpeed = 2.0f;
    [SerializeField] float LimitLeft = -2.0f;
    [SerializeField] float LimitRight = 2.0f;
    Vector3 startPos;
    Vector3 dist;

    void OnMouseDown()
    {
        startPos = Camera.main.WorldToScreenPoint(transform.position);
        dist = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, startPos.z));
    }

    void OnMouseDrag()
    {
        Vector3 lastPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, startPos.z);
        var newPos = Camera.main.ScreenToWorldPoint(lastPos) + dist;
        transform.position = new Vector3(newPos.x, transform.position.y, transform.position.z);
        CorrectCollisionsWithLimits();
    }

    void CorrectCollisionsWithLimits()
    {
        float width = transform.localScale.x / 2;

        if (transform.position.x - width < LimitLeft)
            transform.position = new Vector3(LimitLeft + width, transform.position.y, transform.position.z);

        if (transform.position.x + width > LimitRight)
            transform.position = new Vector3(LimitRight - width, transform.position.y, transform.position.z);
    }
}

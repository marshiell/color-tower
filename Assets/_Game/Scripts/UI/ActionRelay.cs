using UnityEngine;
using UnityEngine.Events;

public class ActionRelay : MonoBehaviour
{
    [SerializeField] bool TriggerOnEnable;
    [SerializeField] UnityEvent actions;

    private void OnEnable()
    {
        if (TriggerOnEnable)
            Trigger();
    }
    public void Trigger()
    {
        if (actions != null)
            actions.Invoke();
    }
}
using System;
using System.Linq;
using UnityEngine;


public enum Menu
{
    Main,
    Win,
    InGame
}

[Serializable]
public class MenuCanvas
{
    public Menu menu;
    public Canvas menuCanvas;
}

public class MenuController : MonoBehaviour
{
    public MenuCanvas[] menus;

    public void LoadMenu(Menu menu)
    {
        var menuToLoad = menus.SingleOrDefault(m => m.menu == menu);

        if (menuToLoad == null) return;

        CloseAllMenus();
        menuToLoad.menuCanvas.gameObject.SetActive(true);
    }

    void CloseAllMenus()
    {
        var canvases = FindObjectsOfType<Canvas>();
        foreach (var canvas in canvases)
            canvas.gameObject.SetActive(false);
    }
}

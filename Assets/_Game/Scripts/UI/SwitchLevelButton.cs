using UnityEngine;

public class SwitchLevelButton : MonoBehaviour
{
    GameController gameController;
    public void StartNextWorld()
    {
        if (gameController == null) gameController = FindObjectOfType<GameController>();
        gameController.StartNextWorld();
    }
}

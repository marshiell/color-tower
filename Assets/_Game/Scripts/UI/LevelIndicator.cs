using UnityEngine;
using UnityEngine.UI;

public class LevelIndicator : MonoBehaviour
{
    Text levelText;

    void OnEnable()
    {
        UpdateText(LevelControllerSingleton.GetLevelController().CurrentWorld.WorldId);
    }

    public void UpdateText(int worldId)
    {
        if (levelText == null) levelText = GetComponent<Text>();
        if (levelText != null) levelText.text = "LEVEL " + worldId;

        var popupEffect = transform.parent.GetComponent<PopupEffect>();
        if (popupEffect != null) popupEffect.StartOpen();
    }
}

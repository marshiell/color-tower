using System.Collections;
using UnityEngine;

public class PopupEffect : MonoBehaviour
{
    [SerializeField] bool openOnEnable = true;
    [SerializeField] bool closeOnOpen;
    [SerializeField] float DelayBeforeOpen;
    [SerializeField] float DelayBeforeClose;
    [SerializeField] float OpenDuration = 0.3f;
    [SerializeField] float CloseDuration = 0.15f;
    [SerializeField] GameObject[] NextGameObjectsToActivate;
    [SerializeField] float DurationBeforeOpenNextPopup;
    [SerializeField] Vector2 scaleTarget = Vector2.one;

    bool closing = false;
    bool opening = false;
    GameObject gameObjectToDisableAfterClose;

    void OnEnable()
    {
        if (openOnEnable) StartOpen();
    }

    public void StartOpen()
    {
        opening = true;

        transform.localScale = Vector2.zero;
        StartCoroutine(OpenAfterDelay());

        if (closeOnOpen) StartClose();
    }

    public void StartClose()
    {
        StartCoroutine(CloseAfterDelay());
    }

    IEnumerator OpenAfterDelay()
    {
        yield return new WaitForSeconds(DelayBeforeOpen);
        Open();
        yield return null;
    }

    IEnumerator CloseAfterDelay()
    {
        yield return new WaitForSeconds(DelayBeforeClose);
        Close(null);
        yield return null;
    }

    void Open()
    {
        transform.LeanScale(scaleTarget, OpenDuration).setEaseOutBack();
    }

    public void Close(GameObject gameObjectToDisableAfterClose)
    {
        this.gameObjectToDisableAfterClose = gameObjectToDisableAfterClose;

        closing = true;

        transform.LeanScale(Vector2.zero, CloseDuration);
    }

    void Update()
    {
        if (closing)
        {
            if((Vector2)transform.localScale == Vector2.zero)
            {
                closing = false;
                if (gameObjectToDisableAfterClose != null) gameObjectToDisableAfterClose.SetActive(false);
            }    
        }

        if (opening)
        {
            if((Vector2)transform.localScale == scaleTarget)
            {
                opening = false;

                if (NextGameObjectsToActivate.Length > 0)
                    StartCoroutine(StartNextPopup(DurationBeforeOpenNextPopup));
            }    
        }
    }

    IEnumerator StartNextPopup(float timeBeforeStart)
    {
        yield return new WaitForSeconds(timeBeforeStart);
        foreach (var go in NextGameObjectsToActivate)
            if (go != null) go.gameObject.SetActive(true);
        yield return null;
    }
}

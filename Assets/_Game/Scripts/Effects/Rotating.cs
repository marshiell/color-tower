﻿using UnityEngine;

public class Rotating : MonoBehaviour
{
    [SerializeField] Vector3 direction;
    [SerializeField] bool WorldRotation;

    private Vector3 internalRotation;

    void Update()
    {
        Vector3 rotateStep = direction * 10 * Time.deltaTime;
        if (WorldRotation)
        {
            internalRotation += rotateStep;
            transform.eulerAngles = Vector3.zero;
            transform.Rotate(internalRotation);
        }
        else
            transform.Rotate(rotateStep);
    }
}

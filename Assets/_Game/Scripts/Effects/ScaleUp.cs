using UnityEngine;

public class ScaleUp : MonoBehaviour
{
    [SerializeField] bool ScaleUpOnStart = true;
    [SerializeField] float ScaleUpTime = 0.5f;

    private Vector3 startScale;
    void Start()
    {
        startScale = transform.localScale;
        if (ScaleUpOnStart) DoScaleUp();
    }

    public void DoScaleUp()
    {
        transform.localScale = Vector3.zero;
        transform.LeanScale(startScale, ScaleUpTime).setEaseOutBack();
    }
}

using UnityEngine;

[CreateAssetMenu(fileName = "World ", menuName = "ScriptableObjects/WorldScriptableObject", order = 2)]
public class WorldScriptableObject : ScriptableObject
{
    public int WorldId;
    //public LevelScriptableObject[] Levels;

    public float GoalLineY = 2;

    public Material SkyboxMaterial;
    public bool Fog;
    public Color FogColor;
    public GameObject backgroundPrefab;

    public Sphere[] spheres;
    public Sprite[] worldTitleSprites;

    private Material renderSettingsSkybox = null;
    private Background background;

    private GameObject goalLineGameObject;

    public void InitializeWorld()
    {
        ApplySkyboxMaterial();
        ApplyFog();
        ApplyBackground();
        ApplyGoalLine();
    }

    void ApplyGoalLine()
    {
        goalLineGameObject = FindObjectOfType<GoalLine>().transform.parent.gameObject;
        goalLineGameObject.transform.position = new Vector3(goalLineGameObject.transform.position.x, GoalLineY, goalLineGameObject.transform.position.z);
    }

    void ApplyBackground()
    {
        if (background == null) background = FindObjectOfType<Background>();
        background.SetBackground(backgroundPrefab, WorldId);
    }
    void ApplySkyboxMaterial()
    {
        if (SkyboxMaterial == null) return;

        if (renderSettingsSkybox == null)
            renderSettingsSkybox = RenderSettings.skybox;
        RenderSettings.skybox = SkyboxMaterial;

        Skybox skybox = (Skybox)FindObjectOfType(typeof(Skybox));
        if (skybox == null) return;

        skybox.material = SkyboxMaterial;
    }

    void ApplyFog()
    {
        RenderSettings.fogColor = FogColor;

        if (!Fog)
            RenderSettings.fog = false;
        else
            RenderSettings.fog = true;

        FindObjectOfType<Camera>().backgroundColor = FogColor;
    }
}
